#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <math.h>
#include <windows.h>

#define MAX 10 // 스택과 큐의 최대 크기
#define SIZE 10 // 원형 큐의 크기
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// 이진 트리 노드 구조체
typedef struct Node {
    int data;
    struct Node* left;
    struct Node* right;
} Node;

// 새 노드 생성 함수
Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
}

// 이진 트리에 노드 추가 함수
Node* insertNode(Node* root, int data) {
    if (root == NULL) {
        root = createNode(data);
    }
    else if (data < root->data) {
        root->left = insertNode(root->left, data);
    }
    else {
        root->right = insertNode(root->right, data);
    }
    return root;
}

// 전위 순회 함수
void preorderTraversal(Node* root) {
    if (root != NULL) {
        printf("%d ", root->data);
        preorderTraversal(root->left);
        preorderTraversal(root->right);
    }
}

// 중위 순회 함수
void inorderTraversal(Node* root) {
    if (root != NULL) {
        inorderTraversal(root->left);
        printf("%d ", root->data);
        inorderTraversal(root->right);
    }
}

// 후위 순회 함수
void postorderTraversal(Node* root) {
    if (root != NULL) {
        postorderTraversal(root->left);
        postorderTraversal(root->right);
        printf("%d ", root->data);
    }
}

// 큐 노드 구조체 정의
typedef struct QueueNode {
    Node* treeNode;
    struct QueueNode* next;
} QueueNode;

// 큐 구조체 정의
typedef struct Queue {
    QueueNode* front, * rear;
} Queue;

// 큐 초기화 함수
Queue* createQueue() {
    Queue* queue = (Queue*)malloc(sizeof(Queue));
    queue->front = queue->rear = NULL;
    return queue;
}

// 큐가 비어 있는지 확인하는 함수
int isQueueEmpty(Queue* queue) {
    return queue->front == NULL;
}

// 큐에 삽입하는 함수
void enqueue(Queue* queue, Node* node) {
    QueueNode* temp = (QueueNode*)malloc(sizeof(QueueNode));
    temp->treeNode = node;
    temp->next = NULL;
    if (queue->rear == NULL) {
        queue->front = queue->rear = temp;
        return;
    }
    queue->rear->next = temp;
    queue->rear = temp;
}

// 큐에서 삭제하는 함수
Node* dequeue(Queue* queue) {
    if (isQueueEmpty(queue))
        return NULL;
    QueueNode* temp = queue->front;
    Node* node = temp->treeNode;
    queue->front = queue->front->next;
    if (queue->front == NULL)
        queue->rear = NULL;
    free(temp);
    return node;
}

// 레벨 순회 함수
void levelOrder(Node* root) {
    if (root == NULL)
        return;

    Queue* queue = createQueue();
    enqueue(queue, root);

    while (!isQueueEmpty(queue)) {
        Node* node = dequeue(queue);
        printf("%d ", node->data);

        if (node->left != NULL)
            enqueue(queue, node->left);

        if (node->right != NULL)
            enqueue(queue, node->right);
    }
    free(queue);
}

// 사용자 입력을 통해 트리 구성
Node* insertNodeFromInput() {
    int data;
    printf("노드 값을 입력하세요 (-1 입력 시 노드 없음): ");
    scanf_s("%d", &data);

    if (data == -1)
        return NULL;

    Node* node = createNode(data);
    printf("%d의 왼쪽 자식을 입력하세요:\n", data);
    node->left = insertNodeFromInput();
    printf("%d의 오른쪽 자식을 입력하세요:\n", data);
    node->right = insertNodeFromInput();

    return node;
}

// 이진 탐색 트리에 새 노드 삽입 함수
Node* insert(Node* node, int data) {
    if (node == NULL) return createNode(data);

    if (data < node->data)
        node->left = insert(node->left, data);
    else if (data > node->data)
        node->right = insert(node->right, data);

    return node;
}

// 이진 탐색 트리에서 노드 탐색 함수
Node* search(Node* root, int data) {
    if (root == NULL || root->data == data)
        return root;

    if (root->data > data)
        return search(root->left, data);

    return search(root->right, data);
}

// 이진 탐색 트리에서 최소값을 가지는 노드를 찾는 함수
Node* minValueNode(Node* node) {
    Node* current = node;
    while (current && current->left != NULL)
        current = current->left;

    return current;
}

// 이진 탐색 트리에서 노드를 삭제하는 함수
Node* deleteNode(Node* root, int data) {
    if (root == NULL) return root;

    if (data < root->data)
        root->left = deleteNode(root->left, data);
    else if (data > root->data)
        root->right = deleteNode(root->right, data);
    else {
        if (root->left == NULL) {
            Node* temp = root->right;
            free(root);
            return temp;
        }
        else if (root->right == NULL) {
            Node* temp = root->left;
            free(root);
            return temp;
        }

        Node* temp = minValueNode(root->right);

        root->data = temp->data;

        root->right = deleteNode(root->right, temp->data);
    }
    return root;
}

// 중위 순회 함수 (오름차순 정렬 출력)
void inorder(Node* root) {
    if (root != NULL) {
        inorder(root->left);
        printf("%d ", root->data);
        inorder(root->right);
    }
}

// 트리 구조 출력 함수
void printTreeStructure(Node* root, int space) {
    if (root == NULL) return;

    space += 10;

    printTreeStructure(root->right, space);

    printf("\n");
    for (int i = 10; i < space; i++) {
        printf(" ");
    }
    printf("%d\n", root->data);

    printTreeStructure(root->left, space);
}

// 스택 구조체 정의
typedef struct {
    int data[MAX];
    int top;
} Stack;

// 큐 구조체 정의
typedef struct {
    int front;
    int rear;
    int data[MAX];
    int size; // 현재 큐에 있는 요소의 개수
} QueueArray;

// 선형 큐 구조체 정의
typedef struct {
    int front;
    int rear;
    int data[MAX];
} LinearQueue;

// 원형 큐 구조체 정의
typedef struct {
    int items[SIZE];
    int front, rear;
} CircularQueue;

// 덱 노드 정의
typedef struct DequeNode {
    int data;
    struct DequeNode* next;
    struct DequeNode* prev;
} DequeNode;

// 덱 정의
typedef struct {
    DequeNode* front;
    DequeNode* rear;
} Deque;

// 연결 리스트 노드 정의
typedef struct ListNode {
    int data;
    struct ListNode* next;
} ListNode;

// 배열 리스트 정의
typedef struct {
    int data[MAX][MAX];
    int current_line;
    int current_size;
} ArrayList;

// 이중 연결 리스트 노드 정의
typedef int element;
typedef struct DListNode {
    element data;
    struct DListNode* llink;
    struct DListNode* rlink;
} DListNode;

// 함수 프로토타입 선언
void initStack(Stack* s);
int isEmptyStack(Stack* s);
int isFullStack(Stack* s);
void push(Stack* s, int value);
int pop(Stack* s);
int peek(Stack* s);
void visualizeStack(Stack* s);
void runStackConcept();

void initQueue(QueueArray* q);
int isEmptyQueue(QueueArray* q);
int isFullQueue(QueueArray* q);
void enqueueQueue(QueueArray* q, int item);
int dequeueQueue(QueueArray* q);
int frontQueue(QueueArray* q);
void visualizeQueue(QueueArray* q);
void runQueueConcept();

void initLinearQueue(LinearQueue* q);
int isFullLinearQueue(LinearQueue* q);
int isEmptyLinearQueue(LinearQueue* q);
void enqueueLinearQueue(LinearQueue* q, int value);
int dequeueLinearQueue(LinearQueue* q);
void displayLinearQueue(LinearQueue* q);
void visualizeLinearQueue(LinearQueue* q);
void runLinearQueue();

void initializeCircularQueue(CircularQueue* q);
int isFullCircularQueue(CircularQueue* q);
int isEmptyCircularQueue(CircularQueue* q);
void enqueueCircularQueue(CircularQueue* q, int element);
int dequeueCircularQueue(CircularQueue* q);
void displayCircularQueue(CircularQueue* q);
void runCircularQueue();

Deque* createDeque();
void insertFront(Deque* deque, int data);
void insertRear(Deque* deque, int data);
int deleteFront(Deque* deque);
int deleteRear(Deque* deque);
void printDeque(Deque* deque);
void runDeque();

void initArrayList(ArrayList* list);
void addElement(ArrayList* list, int element);
void removeElement(ArrayList* list, int element);
void printArrayList(ArrayList* list);
void runArrayListConcept();

void runLinkedList();
void runSinglyLinkedList();
void runCircularLinkedList();
void runDoublyLinkedList();

void set_color(int color);
void reset_color();
void printTitle();
void printMainMenu(int selection);
void printStackMenu(int selection);
void printQueueMenu(int selection);
void printListMenu(int selection);
void printTreeMenu(int selection);
void printBinaryTreeMenu(int selection);
void printSelectedMenu(const char* menuName);

// 새로운 함수 선언
void visualizeLinkedList(ListNode* head);
void visualizeQueueLinkedList(ListNode* front);
void visualizeDoublyLinkedList(DListNode* head);

// 이진 트리 함수 프로토타입 선언
void runBinaryTree();
void runLevelOrderTraversal();
void runBinarySearchTree();
void runThreadedBinaryTree();

// 쓰레드 이진 트리 노드 정의
typedef struct ThreadedNode {
    int data;
    struct ThreadedNode* left;
    struct ThreadedNode* right;
    int leftThread;
    int rightThread;
} ThreadedNode;

ThreadedNode* dummyNode = NULL;

// 쓰레드 이진 트리 노드 생성 함수
ThreadedNode* createThreadedNode(int data) {
    ThreadedNode* newNode = (ThreadedNode*)malloc(sizeof(ThreadedNode));
    newNode->data = data;
    newNode->left = newNode->right = NULL;
    newNode->leftThread = 1;
    newNode->rightThread = 1;
    return newNode;
}

// 쓰레드 이진 트리에 노드 삽입 함수
ThreadedNode* insertThreadedNode(ThreadedNode* root, int data) {
    ThreadedNode* newNode = createThreadedNode(data);

    if (dummyNode == NULL) {
        dummyNode = createThreadedNode(-1);
        dummyNode->left = dummyNode;
        dummyNode->right = dummyNode;
    }

    if (root == NULL) {
        newNode->left = dummyNode;
        newNode->right = dummyNode;
        return newNode;
    }

    ThreadedNode* current = root;
    ThreadedNode* parent = NULL;

    while (current != dummyNode) {
        parent = current;
        if (data < current->data) {
            if (current->leftThread == 0) {
                current = current->left;
            }
            else {
                break;
            }
        }
        else {
            if (current->rightThread == 0) {
                current = current->right;
            }
            else {
                break;
            }
        }
    }

    if (data < parent->data) {
        newNode->left = parent->left;
        newNode->right = parent;
        parent->leftThread = 0;
        parent->left = newNode;
    }
    else {
        newNode->right = parent->right;
        newNode->left = parent;
        parent->rightThread = 0;
        parent->right = newNode;
    }
    return root;
}

// 쓰레드 이진 트리 중위 순회 함수
void inorderThreadedTraversal(ThreadedNode* root) {
    if (dummyNode == NULL) {
        dummyNode = createThreadedNode(-1);
        dummyNode->left = dummyNode;
        dummyNode->right = dummyNode;
    }

    if (root == NULL) {
        return;
    }

    ThreadedNode* current = root;
    while (current->leftThread == 0) {
        current = current->left;
    }

    while (current != dummyNode) {
        printf("%d ", current->data);
        if (current->rightThread == 1) {
            current = current->right;
        }
        else {
            current = current->right;
            while (current != dummyNode && current->leftThread == 0) {
                current = current->left;
            }
        }
    }
}

// 사용자 입력을 통해 쓰레드 이진 트리 구성 함수
ThreadedNode* insertThreadedNodeFromInput(ThreadedNode* root) {
    int data;
    printf("노드 값을 입력하세요 (-1 입력 시 노드 없음): ");
    scanf_s("%d", &data);

    if (data == -1) {
        return root;
    }

    root = insertThreadedNode(root, data);
    return root;
}

// 쓰레드 이진 트리 실행 함수
void runThreadedBinaryTree() {
    ThreadedNode* root = NULL;
    int choice, value;

    while (1) {
        printf("\n1. 삽입\n2. 중위 순회\n3. 종료\n");
        printf("원하는 기능을 선택하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            root = insertThreadedNodeFromInput(root);
            break;
        case 2:
            printf("중위 순회: ");
            inorderThreadedTraversal(root);
            printf("\n");
            break;
        case 3:
            return;
        default:
            printf("잘못된 선택입니다. 다시 시도하세요.\n");
        }
    }
}

void initStack(Stack* s) {
    s->top = -1;
}

int isEmptyStack(Stack* s) {
    return s->top == -1;
}

int isFullStack(Stack* s) {
    return s->top == MAX - 1;
}

void push(Stack* s, int value) {
    if (isFullStack(s)) {
        printf("스택 오버플로우\n");
        return;
    }
    s->data[++(s->top)] = value;
    visualizeStack(s);
}

int pop(Stack* s) {
    if (isEmptyStack(s)) {
        printf("스택 언더플로우\n");
        return -1;
    }
    int value = s->data[(s->top)--];
    visualizeStack(s);
    return value;
}

int peek(Stack* s) {
    if (isEmptyStack(s)) {
        printf("스택이 비어있습니다\n");
        return -1;
    }
    return s->data[s->top];
}

void visualizeStack(Stack* s) {
    printf("\n현재 스택 상태:\n");
    for (int i = s->top; i >= 0; i--) {
        printf("| %d |\n", s->data[i]);
    }
    if (isEmptyStack(s)) {
        printf("|   |\n");
    }
    printf("-----\n");
}

void runStackConcept() {
    Stack s;
    initStack(&s);

    int choice, value;

    while (1) {
        printf("\n1. 추가\n2. 삭제\n3. 최상위 값\n4. 종료\n");
        printf("선택을 입력하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("추가할 값을 입력하세요: ");
            scanf_s("%d", &value);
            push(&s, value);
            break;
        case 2:
            value = pop(&s);
            if (value != -1) {
                printf("삭제된 값: %d\n", value);
            }
            break;
        case 3:
            value = peek(&s);
            if (value != -1) {
                printf("최상위 값: %d\n", value);
            }
            break;
        case 4:
            return;
        default:
            printf("잘못된 선택입니다. 다시 시도하세요.\n");
        }
    }
}

void initQueue(QueueArray* q) {
    q->front = 0;
    q->rear = 0;
    q->size = 0;
}

int isEmptyQueue(QueueArray* q) {
    return q->size == 0;
}

int isFullQueue(QueueArray* q) {
    return q->size == MAX;
}

void enqueueQueue(QueueArray* q, int item) {
    if (isFullQueue(q)) {
        printf("큐가 가득 찼습니다! 자동으로 맨 앞의 요소를 제거합니다.\n");
        dequeueQueue(q);
    }
    q->data[q->rear] = item;
    q->rear = (q->rear + 1) % MAX;
    q->size++;
    visualizeQueue(q);
}

int dequeueQueue(QueueArray* q) {
    if (isEmptyQueue(q)) {
        printf("큐가 비어 있습니다!\n");
        return -1;
    }
    int item = q->data[q->front];
    q->front = (q->front + 1) % MAX;
    q->size--;
    visualizeQueue(q);
    return item;
}

int frontQueue(QueueArray* q) {
    if (isEmptyQueue(q)) {
        printf("큐가 비어 있습니다!\n");
        return -1;
    }
    return q->data[q->front];
}

void visualizeQueue(QueueArray* q) {
    printf("\n현재 큐 상태:\n");
    printf("큐: [ ");
    for (int i = 0; i < q->size; i++) {
        printf("%d ", q->data[(q->front + i) % MAX]);
    }
    printf("]\n");
    printf("앞: %d, 뒤: %d\n", q->front, q->rear);
}

void runQueueConcept() {
    QueueArray q;
    initQueue(&q);

    int choice, value;

    while (1) {
        printf("\n1. 추가\n2. 제거\n3. 앞 요소 표시\n4. 종료\n");
        printf("선택을 입력하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("추가할 값을 입력하세요: ");
            scanf_s("%d", &value);
            enqueueQueue(&q, value);
            break;
        case 2:
            value = dequeueQueue(&q);
            if (value != -1) {
                printf("제거된 값: %d\n", value);
            }
            break;
        case 3:
            value = frontQueue(&q);
            if (value != -1) {
                printf("앞 요소: %d\n", value);
            }
            break;
        case 4:
            return;
        default:
            printf("잘못된 선택입니다. 다시 시도하세요.\n");
        }
    }
}

void initLinearQueue(LinearQueue* q) {
    q->front = -1;
    q->rear = -1;
}

int isFullLinearQueue(LinearQueue* q) {
    return q->rear == MAX - 1;
}

int isEmptyLinearQueue(LinearQueue* q) {
    return q->front == -1 || q->front > q->rear;
}

void enqueueLinearQueue(LinearQueue* q, int value) {
    if (isFullLinearQueue(q)) {
        printf("큐가 가득 찼습니다\n");
        return;
    }
    if (isEmptyLinearQueue(q)) {
        q->front = 0;
    }
    q->data[++q->rear] = value;
    visualizeLinearQueue(q);
}

int dequeueLinearQueue(LinearQueue* q) {
    if (isEmptyLinearQueue(q)) {
        printf("큐가 비어 있습니다\n");
        return -1;
    }
    int value = q->data[q->front++];
    visualizeLinearQueue(q);
    return value;
}

void displayLinearQueue(LinearQueue* q) {
    if (isEmptyLinearQueue(q)) {
        printf("큐가 비어 있습니다\n");
        return;
    }
    printf("큐: ");
    for (int i = q->front; i <= q->rear; i++) {
        printf("%d ", q->data[i]);
    }
    printf("\n");
}

void visualizeLinearQueue(LinearQueue* q) {
    printf("\n현재 선형 큐 상태:\n");
    for (int i = 0; i < MAX; i++) {
        if (i >= q->front && i <= q->rear) {
            printf("| %2d ", q->data[i]);
        }
        else {
            printf("|    ");
        }
    }
    printf("|\n");
    for (int i = 0; i < MAX; i++) {
        printf("  %d ", i);
    }
    printf("\n\n");
}

void runLinearQueue() {
    LinearQueue q;
    initLinearQueue(&q);

    int choice, value;

    while (1) {
        printf("\n1. 삽입(Enqueue)\n2. 삭제(Dequeue)\n3. 표시(Display)\n4. 종료(Exit)\n");
        printf("선택하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("삽입할 값을 입력하세요: ");
            scanf_s("%d", &value);
            enqueueLinearQueue(&q, value);
            break;
        case 2:
            value = dequeueLinearQueue(&q);
            if (value != -1) {
                printf("삭제된 값: %d\n", value);
            }
            break;
        case 3:
            displayLinearQueue(&q);
            break;
        case 4:
            return;
        default:
            printf("잘못된 선택입니다\n");
        }
    }
}

void initializeCircularQueue(CircularQueue* q) {
    q->front = -1;
    q->rear = -1;
}

int isFullCircularQueue(CircularQueue* q) {
    return (q->front == q->rear + 1) || (q->front == 0 && q->rear == SIZE - 1);
}

int isEmptyCircularQueue(CircularQueue* q) {
    return q->front == -1;
}

void enqueueCircularQueue(CircularQueue* q, int element) {
    if (isFullCircularQueue(q)) {
        printf("\n큐가 가득 찼습니다!!\n");
    }
    else {
        if (q->front == -1) q->front = 0;
        q->rear = (q->rear + 1) % SIZE;
        q->items[q->rear] = element;
        printf("\n삽입된 요소 -> %d\n", element);
        displayCircularQueue(q);
    }
}

int dequeueCircularQueue(CircularQueue* q) {
    int element;
    if (isEmptyCircularQueue(q)) {
        printf("\n큐가 비어 있습니다!! \n");
        return -1;
    }
    else {
        element = q->items[q->front];
        if (q->front == q->rear) {
            q->front = -1;
            q->rear = -1;
        }
        else {
            q->front = (q->front + 1) % SIZE;
        }
        printf("\n삭제된 요소 -> %d \n", element);
        displayCircularQueue(q);
        return element;
    }
}

void displayCircularQueue(CircularQueue* q) {
    int radius = 10;
    int diameter = 2 * radius + 1;
    char canvas[100][100];
    int centerX = radius, centerY = radius;

    for (int i = 0; i < diameter; i++) {
        for (int j = 0; j < diameter; j++) {
            canvas[i][j] = ' ';
        }
    }

    if (!isEmptyCircularQueue(q)) {
        int numElements = (q->rear + SIZE - q->front) % SIZE + 1;
        for (int i = 0; i < numElements; i++) {
            double angle = 2 * M_PI * i / numElements;
            int x = centerX + (int)(radius * cos(angle));
            int y = centerY + (int)(radius * sin(angle));
            int index = (q->front + i) % SIZE;
            if (x >= 0 && x < diameter && y >= 0 && y < diameter) {
                char buf[5];
                snprintf(buf, sizeof(buf), "%d", q->items[index]);
                for (int k = 0; k < strlen(buf); k++) {
                    canvas[y][x + k] = buf[k];
                }
            }
        }
    }

    printf("\n원형 큐 상태:\n");
    for (int i = 0; i < diameter; i++) {
        for (int j = 0; j < diameter; j++) {
            printf("%c", canvas[i][j]);
        }
        printf("\n");
    }
}

void runCircularQueue() {
    CircularQueue q;
    initializeCircularQueue(&q);

    int choice, value;

    while (1) {
        printf("\n1. 요소 삽입(Enqueue)\n");
        printf("2. 요소 제거(Dequeue)\n");
        printf("3. 종료(Exit)\n");
        printf("선택을 입력하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("삽입할 값을 입력하세요: ");
            scanf_s("%d", &value);
            enqueueCircularQueue(&q, value);
            break;
        case 2:
            dequeueCircularQueue(&q);
            break;
        case 3:
            return;
        default:
            printf("잘못된 선택입니다! 유효한 선택을 입력하세요.\n");
        }
    }
}

Deque* createDeque() {
    Deque* deque = (Deque*)malloc(sizeof(Deque));
    deque->front = NULL;
    deque->rear = NULL;
    return deque;
}

void insertFront(Deque* deque, int data) {
    DequeNode* newNode = (DequeNode*)malloc(sizeof(DequeNode));
    newNode->data = data;
    newNode->next = deque->front;
    newNode->prev = NULL;
    if (deque->front == NULL) {
        deque->rear = newNode;
    }
    else {
        deque->front->prev = newNode;
    }
    deque->front = newNode;
    printDeque(deque);
}

void insertRear(Deque* deque, int data) {
    DequeNode* newNode = (DequeNode*)malloc(sizeof(DequeNode));
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = deque->rear;
    if (deque->rear == NULL) {
        deque->front = newNode;
    }
    else {
        deque->rear->next = newNode;
    }
    deque->rear = newNode;
    printDeque(deque);
}

int deleteFront(Deque* deque) {
    if (deque->front == NULL) {
        printf("덱이 비어 있습니다\n");
        return -1;
    }
    DequeNode* temp = deque->front;
    int data = temp->data;
    deque->front = deque->front->next;
    if (deque->front == NULL) {
        deque->rear = NULL;
    }
    else {
        deque->front->prev = NULL;
    }
    free(temp);
    printDeque(deque);
    return data;
}

int deleteRear(Deque* deque) {
    if (deque->rear == NULL) {
        printf("덱이 비어 있습니다\n");
        return -1;
    }
    DequeNode* temp = deque->rear;
    int data = temp->data;
    deque->rear = deque->rear->prev;
    if (deque->rear == NULL) {
        deque->front = NULL;
    }
    else {
        deque->rear->next = NULL;
    }
    free(temp);
    printDeque(deque);
    return data;
}

void printDeque(Deque* deque) {
    DequeNode* temp = deque->front;
    printf("덱 상태:\n");
    while (temp != NULL) {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

void runDeque() {
    Deque* deque = createDeque();
    int command;
    int value;

    while (1) {
        printf("\n명령어:\n");
        printf("1: 앞에 삽입 <값>\n");
        printf("2: 뒤에 삽입 <값>\n");
        printf("3: 앞에서 제거\n");
        printf("4: 뒤에서 제거\n");
        printf("5: 종료\n");
        printf("번호를 입력하세요: ");
        scanf_s("%d", &command);
        switch (command) {
        case 1:
            printf("값을 입력하세요: ");
            scanf_s("%d", &value);
            insertFront(deque, value);
            break;
        case 2:
            printf("값을 입력하세요: ");
            scanf_s("%d", &value);
            insertRear(deque, value);
            break;
        case 3:
            deleteFront(deque);
            break;
        case 4:
            deleteRear(deque);
            break;
        case 5:
            printf("프로그램을 종료합니다.\n");
            while (deque->front != NULL) {
                deleteFront(deque);
            }
            free(deque);
            return;
        default:
            printf("잘못된 명령어입니다.\n");
        }
    }
}

void initArrayList(ArrayList* list) {
    list->current_line = 0;
    list->current_size = 0;
    for (int i = 0; i < MAX; i++) {
        for (int j = 0; j < MAX; j++) {
            list->data[i][j] = 0;
        }
    }
}

void addElement(ArrayList* list, int element) {
    if (list->current_size >= MAX) {
        if (list->current_line >= MAX - 1) {
            printf("리스트가 가득 찼습니다!\n");
            return;
        }
        list->current_line++;
        list->current_size = 0;
    }
    list->data[list->current_line][list->current_size++] = element;
    printArrayList(list);
}

void removeElement(ArrayList* list, int element) {
    int found = 0;
    for (int i = 0; i <= list->current_line; i++) {
        for (int j = 0; j < MAX; j++) {
            if (list->data[i][j] == element) {
                found = 1;
                for (int k = j; k < MAX - 1; k++) {
                    list->data[i][k] = list->data[i][k + 1];
                }
                if (i < list->current_line) {
                    list->data[i][MAX - 1] = list->data[i + 1][0];
                    for (int k = 0; k < MAX - 1; k++) {
                        list->data[i + 1][k] = list->data[i + 1][k + 1];
                    }
                    list->data[i + 1][MAX - 1] = 0;
                }
                else {
                    list->data[i][MAX - 1] = 0;
                }
                if (list->current_size == 0) {
                    if (list->current_line > 0) {
                        list->current_line--;
                        list->current_size = MAX - 1;
                    }
                    else {
                        list->current_size = 0;
                    }
                }
                else {
                    list->current_size--;
                }
                break;
            }
        }
        if (found) break;
    }
    if (found) {
        printf("요소 %d가 삭제되었습니다.\n", element);
    }
    else {
        printf("요소 %d를 찾을 수 없습니다.\n", element);
    }
    printArrayList(list);
}

void printArrayList(ArrayList* list) {
    printf("리스트 상태\n\n");
    for (int i = 0; i <= list->current_line; i++) {
        for (int j = 0; j < MAX; j++) {
            if (i == list->current_line && j >= list->current_size) break;
            printf("%d ", list->data[i][j]);
        }
        printf("\n");
    }
}

void runArrayListConcept() {
    ArrayList list;
    initArrayList(&list);

    int choice, element;

    while (1) {
        printf("\n1. 요소 추가\n2. 요소 삭제\n3. 요소 표시\n4. 종료\n");
        printf("선택을 입력하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("추가할 요소를 입력하세요: ");
            scanf_s("%d", &element);
            printf("\n");
            addElement(&list, element);
            break;
        case 2:
            printf("삭제할 요소를 입력하세요: ");
            scanf_s("%d", &element);
            removeElement(&list, element);
            break;
        case 3:
            printArrayList(&list);
            break;
        case 4:
            return;
        default:
            printf("잘못된 선택입니다!\n");
        }
        printf("\n\n");
    }
}

void runLinkedList() {
    ListNode* head = NULL;
    ListNode* temp;
    int choice, value, index;

    while (1) {
        printf("\n1. 요소 추가\n2. 요소 삭제\n3. 종료\n");
        printf("선택을 입력하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("추가할 값을 입력하세요: ");
            scanf_s("%d", &value);
            temp = (ListNode*)malloc(sizeof(ListNode));
            temp->data = value;
            temp->next = head;
            head = temp;
            visualizeLinkedList(head); // 추가 후 시각화
            break;
        case 2:
            if (head == NULL) {
                printf("리스트가 비어 있습니다\n");
            }
            else {
                printf("삭제할 인덱스를 입력하세요: ");
                scanf_s("%d", &index);
                ListNode* prev = NULL;
                temp = head;
                for (int i = 0; i < index && temp != NULL; i++) {
                    prev = temp;
                    temp = temp->next;
                }
                if (temp == NULL) {
                    printf("잘못된 인덱스입니다\n");
                }
                else {
                    if (prev == NULL) {
                        head = temp->next;
                    }
                    else {
                        prev->next = temp->next;
                    }
                    free(temp);
                    visualizeLinkedList(head); // 삭제 후 시각화
                }
            }
            break;
        case 3:
            visualizeLinkedList(head); // 현재 상태 시각화
            break;
        case 4:
            while (head != NULL) {
                temp = head;
                head = head->next;
                free(temp);
            }
            return; // 종료
        default:
            printf("잘못된 선택입니다\n");
        }
    }
}

void visualizeLinkedList(ListNode* head) {
    printf("\n현재 연결 리스트 상태:\n");
    ListNode* temp = head;
    while (temp != NULL) {
        printf("%d -> ", temp->data);
        temp = temp->next;
    }
    printf("NULL\n");
}

void runSinglyLinkedList() {
    runLinkedList();
}

void runCircularLinkedList() {
    ListNode* head = NULL;
    ListNode* temp;
    int choice, value;

    while (1) {
        printf("\n1. 요소 추가\n2. 요소 삭제\n3. 요소 표시\n4. 종료\n");
        printf("선택을 입력하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("추가할 값을 입력하세요: ");
            scanf_s("%d", &value);
            temp = (ListNode*)malloc(sizeof(ListNode));
            temp->data = value;
            if (head == NULL) {
                head = temp;
                head->next = head;
            }
            else {
                ListNode* tail = head;
                while (tail->next != head) {
                    tail = tail->next;
                }
                tail->next = temp;
                temp->next = head;
            }
            break;
        case 2:
            if (head == NULL) {
                printf("리스트가 비어 있습니다\n");
            }
            else {
                printf("삭제할 값을 입력하세요: ");
                scanf_s("%d", &value);
                ListNode* prev = NULL;
                temp = head;
                do {
                    if (temp->data == value) {
                        if (prev == NULL) {
                            ListNode* tail = head;
                            while (tail->next != head) {
                                tail = tail->next;
                            }
                            if (head == head->next) {
                                head = NULL;
                            }
                            else {
                                head = head->next;
                                tail->next = head;
                            }
                            free(temp);
                        }
                        else {
                            prev->next = temp->next;
                            if (temp == head) {
                                head = temp->next;
                            }
                            free(temp);
                        }
                        break;
                    }
                    prev = temp;
                    temp = temp->next;
                } while (temp != head);
            }
            break;
        case 3:
            if (head == NULL) {
                printf("리스트가 비어 있습니다\n");
            }
            else {
                printf("리스트: ");
                temp = head;
                do {
                    printf("%d -> ", temp->data);
                    temp = temp->next;
                } while (temp != head);
                printf("HEAD\n");
            }
            break;
        case 4:
            if (head != NULL) {
                ListNode* tail = head;
                while (head != NULL) {
                    temp = head;
                    head = head->next;
                    free(temp);
                }
            }
            return;
        default:
            printf("잘못된 선택입니다\n");
        }
    }
}

void initDoublyLinkedList(DListNode* phead) {
    phead->llink = phead;
    phead->rlink = phead;
}

void printDoublyLinkedList(DListNode* phead) {
    DListNode* p;
    for (p = phead->rlink; p != phead; p = p->rlink) {
        printf("<-| |%d| |-> ", p->data);
    }
    printf("\n");
}

void insertDoublyLinkedList(DListNode* before, element data) {
    DListNode* newnode = (DListNode*)malloc(sizeof(DListNode));
    if (newnode == NULL) {
        fprintf(stderr, "메모리 할당 오류\n");
        exit(1);
    }
    newnode->data = data;
    newnode->llink = before;
    newnode->rlink = before->rlink;
    before->rlink->llink = newnode;
    before->rlink = newnode;
}

void deleteDoublyLinkedList(DListNode* head, DListNode* removed) {
    if (removed == head) {
        return;
    }
    removed->llink->rlink = removed->rlink;
    removed->rlink->llink = removed->llink;
    free(removed);
}

void visualizeDoublyLinkedList(DListNode* head) {
    DListNode* p = head->rlink;
    printf("\n현재 이중 연결 리스트 상태:\n");
    while (p != head) {
        printf(" <-| %d |-> ", p->data);
        p = p->rlink;
    }
    printf("\n");
}

void runDoublyLinkedList() {
    DListNode* head = (DListNode*)malloc(sizeof(DListNode));
    if (head == NULL) {
        fprintf(stderr, "메모리 할당 오류\n");
        exit(1);
    }
    initDoublyLinkedList(head);

    int choice, value;

    while (1) {
        printf("\n1. 요소 추가\n2. 요소 삭제\n3. 요소 표시\n4. 종료\n");
        printf("선택을 입력하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("추가할 값을 입력하세요: ");
            scanf_s("%d", &value);
            insertDoublyLinkedList(head, value);
            visualizeDoublyLinkedList(head); // 시각화
            break;
        case 2:
            if (head->rlink == head) {
                printf("리스트가 비어 있습니다\n");
            }
            else {
                printf("삭제할 값을 입력하세요: ");
                scanf_s("%d", &value);
                DListNode* nodeToDelete = head->rlink;
                while (nodeToDelete != head && nodeToDelete->data != value) {
                    nodeToDelete = nodeToDelete->rlink;
                }
                if (nodeToDelete != head) {
                    deleteDoublyLinkedList(head, nodeToDelete);
                    visualizeDoublyLinkedList(head); // 시각화
                }
                else {
                    printf("값을 찾을 수 없습니다.\n");
                }
            }
            break;
        case 3:
            visualizeDoublyLinkedList(head); // 시각화
            break;
        case 4:
            while (head->rlink != head) {
                deleteDoublyLinkedList(head, head->rlink);
            }
            free(head);
            return;
        default:
            printf("잘못된 선택입니다\n");
        }
    }
}

// 우선순위 큐 요소 정의
typedef struct {
    int priority;  // 우선순위
    int data;      // 데이터
} Element;

// 우선순위 큐 구조체 정의
typedef struct {
    Element* elements;  // 요소 배열
    int size;           // 현재 크기
    int capacity;       // 용량
} PriorityQueue;

// 두 요소를 교환하는 함수
void swap(Element* a, Element* b) {
    Element temp = *a;
    *a = *b;
    *b = temp;
}

// 요소를 위로 힙 정렬하는 함수
void heapify_up(PriorityQueue* pq, int index) {
    if (index && pq->elements[(index - 1) / 2].priority < pq->elements[index].priority) {
        swap(&pq->elements[(index - 1) / 2], &pq->elements[index]);
        heapify_up(pq, (index - 1) / 2);
    }
}

// 요소를 아래로 힙 정렬하는 함수
void heapify_down(PriorityQueue* pq, int index) {
    int left = 2 * index + 1;  // 왼쪽 자식 인덱스
    int right = 2 * index + 2; // 오른쪽 자식 인덱스
    int largest = index;

    if (left < pq->size && pq->elements[left].priority > pq->elements[largest].priority) {
        largest = left;
    }

    if (right < pq->size && pq->elements[right].priority > pq->elements[largest].priority) {
        largest = right;
    }

    if (largest != index) {
        swap(&pq->elements[largest], &pq->elements[index]);
        heapify_down(pq, largest);
    }
}

// 우선순위 큐를 초기화하는 함수
PriorityQueue* init_priority_queue(int capacity) {
    PriorityQueue* pq = (PriorityQueue*)malloc(sizeof(PriorityQueue));
    pq->elements = (Element*)malloc(capacity * sizeof(Element));
    pq->size = 0;
    pq->capacity = capacity;
    return pq;
}

// 요소를 삽입하는 함수
void pq_insert(PriorityQueue* pq, int priority, int data) {
    if (pq->size == pq->capacity) {
        printf("우선순위 큐가 가득 찼습니다\n");
        return;
    }

    Element element;
    element.priority = priority;
    element.data = data;
    pq->elements[pq->size] = element;
    heapify_up(pq, pq->size);
    pq->size++;
}

// 최대 우선순위를 가진 요소를 추출하는 함수
Element extract_max(PriorityQueue* pq) {
    if (pq->size == 0) {
        printf("우선순위 큐가 비어 있습니다\n");
        exit(EXIT_FAILURE);
    }

    Element max_element = pq->elements[0];
    pq->elements[0] = pq->elements[--pq->size];
    heapify_down(pq, 0);

    return max_element;
}

// 최대 우선순위를 가진 요소를 조회하는 함수
Element peek_max(PriorityQueue* pq) {
    if (pq->size == 0) {
        printf("우선순위 큐가 비어 있습니다\n");
        exit(EXIT_FAILURE);
    }

    return pq->elements[0];
}

// 동적 할당된 메모리를 해제하는 함수
void free_priority_queue(PriorityQueue* pq) {
    free(pq->elements);
    free(pq);
}

// 우선순위 큐 실행 함수
void runPriorityQueue() {
    int capacity = 10;
    PriorityQueue* pq = init_priority_queue(capacity);

    int choice, priority, data;

    while (1) {
        printf("\n1. 요소 삽입\n2. 최대 우선순위 요소 추출\n3. 최대 우선순위 요소 조회\n4. 종료\n");
        printf("원하는 기능을 선택하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("우선순위와 데이터를 입력하세요: ");
            scanf_s("%d %d", &priority, &data);
            pq_insert(pq, priority, data);
            break;
        case 2:
            if (pq->size > 0) {
                Element max = extract_max(pq);
                printf("추출된 최대 우선순위 요소: %d (우선순위: %d)\n", max.data, max.priority);
            }
            else {
                printf("우선순위 큐가 비어 있습니다\n");
            }
            break;
        case 3:
            if (pq->size > 0) {
                Element max = peek_max(pq);
                printf("최대 우선순위 요소: %d (우선순위: %d)\n", max.data, max.priority);
            }
            else {
                printf("우선순위 큐가 비어 있습니다\n");
            }
            break;
        case 4:
            free_priority_queue(pq);
            return;
        default:
            printf("잘못된 선택입니다. 다시 시도하세요.\n");
        }
    }
}

void set_color(int color) {
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, color);
}

void reset_color() {
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
}

void printTitle() {
    set_color(FOREGROUND_BLUE | FOREGROUND_INTENSITY);
    printf("******************************************************************\n");
    printf("*                                                                *\n");
    printf("*                            자료 구조                           *\n");
    printf("*                                                                *\n");
    printf("******************************************************************\n");
    reset_color();
}

void printMainMenu(int selection) {
    system("cls");
    printTitle();
    printf("\n-------------------------- 메인 메뉴 ---------------------------\n");
    const char* menuItems[] = { "스택", "큐", "리스트", "트리", "우선순위 큐" };
    for (int i = 0; i < 5; i++) {
        if (selection == i + 1) {
            set_color(FOREGROUND_GREEN | FOREGROUND_INTENSITY);
            printf("-> %s\n", menuItems[i]);
            reset_color();
        }
        else {
            printf("   %s\n", menuItems[i]);
        }
    }
    printf("------------------------------------------------------------------\n");
    printf("방향키로 이동하고 Enter 키로 선택하세요\n");
}

void printStackMenu(int selection) {
    system("cls");
    printTitle();
    printf("\n-------------------------- 스택 메뉴 --------------------------\n");
    const char* stackMenuItems[] = { "스택의 개념" };
    for (int i = 0; i < 1; i++) {
        if (selection == i + 1) {
            set_color(FOREGROUND_GREEN | FOREGROUND_INTENSITY);
            printf("-> %s\n", stackMenuItems[i]);
            reset_color();
        }
        else {
            printf("   %s\n", stackMenuItems[i]);
        }
    }
    printf("------------------------------------------------------------------\n");
    printf("방향키로 이동하고 Enter 키로 선택하세요\n");
}

void printQueueMenu(int selection) {
    system("cls");
    printTitle();
    printf("\n-------------------------- 큐 메뉴 --------------------------\n");
    const char* queueMenuItems[] = { "큐의 개념", "선형 큐", "원형 큐", "덱" };
    for (int i = 0; i < 4; i++) {
        if (selection == i + 1) {
            set_color(FOREGROUND_GREEN | FOREGROUND_INTENSITY);
            printf("-> %s\n", queueMenuItems[i]);
            reset_color();
        }
        else {
            printf("   %s\n", queueMenuItems[i]);
        }
    }
    printf("------------------------------------------------------------------\n");
    printf("방향키로 이동하고 Enter 키로 선택하세요\n");
}

void printListMenu(int selection) {
    system("cls");
    printTitle();
    printf("\n-------------------------- 리스트 메뉴 --------------------------\n");
    const char* listMenuItems[] = {
        "배열로 구현된 리스트",
        "연결 리스트",
        "단순 연결 리스트",
        "원형 연결 리스트",
        "이중 연결 리스트"
    };
    for (int i = 0; i < 5; i++) {
        if (selection == i + 1) {
            set_color(FOREGROUND_GREEN | FOREGROUND_INTENSITY);
            printf("-> %s\n", listMenuItems[i]);
            reset_color();
        }
        else {
            printf("   %s\n", listMenuItems[i]);
        }
    }
    printf("------------------------------------------------------------------\n");
    printf("방향키로 이동하고 Enter 키로 선택하세요\n");
}

void printTreeMenu(int selection) {
    system("cls");
    printTitle();
    printf("\n-------------------------- 트리 메뉴 --------------------------\n");
    const char* treeMenuItems[] = {
        "이진 트리",
        "레벨 순회",
        "이진 탐색 트리",
        "쓰레드 이진 트리"
    };
    for (int i = 0; i < 4; i++) {
        if (selection == i + 1) {
            set_color(FOREGROUND_GREEN | FOREGROUND_INTENSITY);
            printf("-> %s\n", treeMenuItems[i]);
            reset_color();
        }
        else {
            printf("   %s\n", treeMenuItems[i]);
        }
    }
    printf("------------------------------------------------------------------\n");
    printf("방향키로 이동하고 Enter 키로 선택하세요\n");
}

void printSelectedMenu(const char* menuName) {
    system("cls");
    printTitle();
    printf("\n-------------------------- %s --------------------------\n", menuName);
    printf("\n%s 메뉴가 선택되었습니다.\n", menuName);
    printf("\n계속하려면 아무 키나 누르세요...");
    _getch();
}

// 이진 트리 함수 구현
void runBinaryTree() {
    Node* root = NULL;
    int choice, value;

    while (1) {
        printf("\n1. 삽입\n2. 전위 순회\n3. 중위 순회\n4. 후위 순회\n5. 종료\n");
        printf("원하는 기능을 선택하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("삽입할 값을 입력하세요: ");
            scanf_s("%d", &value);
            root = insertNode(root, value);
            break;
        case 2:
            printf("전위 순회: ");
            preorderTraversal(root);
            printf("\n");
            break;
        case 3:
            printf("중위 순회: ");
            inorderTraversal(root);
            printf("\n");
            break;
        case 4:
            printf("후위 순회: ");
            postorderTraversal(root);
            printf("\n");
            break;
        case 5:
            return;
        default:
            printf("잘못된 선택입니다. 다시 시도하세요.\n");
        }
    }
}

// 레벨 순회 함수 구현
void runLevelOrderTraversal() {
    Node* root = NULL;
    int choice;

    while (1) {
        printf("\n1. 이진 트리 생성\n2. 레벨 순회 출력\n3. 종료\n");
        printf("선택을 입력하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("이진 트리를 생성하세요:\n");
            root = insertNodeFromInput();
            break;
        case 2:
            printf("레벨 순회 결과: ");
            levelOrder(root);
            printf("\n");
            break;
        case 3:
            return;
        default:
            printf("잘못된 선택입니다. 다시 시도하세요.\n");
            break;
        }
    }
}

// 이진 탐색 트리 함수 구현
void runBinarySearchTree() {
    Node* root = NULL;
    int choice, value;

    while (1) {
        printf("\n1. 삽입\n2. 중위 순회\n3. 삭제\n4. 검색\n5. 종료\n");
        printf("원하는 기능을 선택하세요: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("삽입할 값을 입력하세요: ");
            scanf_s("%d", &value);
            root = insert(root, value);
            break;
        case 2:
            printf("중위 순회: ");
            inorder(root);
            printf("\n");
            break;
        case 3:
            printf("삭제할 값을 입력하세요: ");
            scanf_s("%d", &value);
            root = deleteNode(root, value);
            printf("중위 순회: ");
            inorder(root);
            printf("\n");
            break;
        case 4:
            printf("검색할 값을 입력하세요: ");
            scanf_s("%d", &value);
            Node* result = search(root, value);
            if (result != NULL) {
                printf("값 %d가 트리에 존재합니다.\n", value);
            }
            else {
                printf("값 %d가 트리에 존재하지 않습니다.\n");
            }
            break;
        case 5:
            return;
        default:
            printf("잘못된 선택입니다. 다시 시도하세요.\n");
        }
    }
}

int main() {
    int selection = 1;
    int stackSelection = 1;
    int queueSelection = 1;
    int listSelection = 1;
    int treeSelection = 1;
    int inStackMenu = 0;
    int inQueueMenu = 0;
    int inListMenu = 0;
    int inTreeMenu = 0;
    int ch;

    while (1) {
        if (!inStackMenu && !inQueueMenu && !inListMenu && !inTreeMenu) {
            printMainMenu(selection);
        }
        else if (inStackMenu) {
            printStackMenu(stackSelection);
        }
        else if (inQueueMenu) {
            printQueueMenu(queueSelection);
        }
        else if (inListMenu) {
            printListMenu(listSelection);
        }
        else if (inTreeMenu) {
            printTreeMenu(treeSelection);
        }

        ch = _getch();
        if (ch == 0 || ch == 224) {
            ch = _getch();
            switch (ch) {
            case 72:
                if (!inStackMenu && !inQueueMenu && !inListMenu && !inTreeMenu) {
                    if (selection > 1) selection--;
                }
                else if (inStackMenu) {
                    if (stackSelection > 1) stackSelection--;
                }
                else if (inQueueMenu) {
                    if (queueSelection > 1) queueSelection--;
                }
                else if (inListMenu) {
                    if (listSelection > 1) listSelection--;
                }
                else if (inTreeMenu) {
                    if (treeSelection > 1) treeSelection--;
                }
                break;
            case 80:
                if (!inStackMenu && !inQueueMenu && !inListMenu && !inTreeMenu) {
                    if (selection < 5) selection++;
                }
                else if (inStackMenu) {
                    if (stackSelection < 1) stackSelection++;
                }
                else if (inQueueMenu) {
                    if (queueSelection < 4) queueSelection++;
                }
                else if (inListMenu) {
                    if (listSelection < 5) listSelection++;
                }
                else if (inTreeMenu) {
                    if (treeSelection < 4) treeSelection++;
                }
                break;
            }
        }
        else if (ch == 13) {
            if (!inStackMenu && !inQueueMenu && !inListMenu && !inTreeMenu) {
                switch (selection) {
                case 1:
                    inStackMenu = 1;
                    break;
                case 2:
                    inQueueMenu = 1;
                    break;
                case 3:
                    inListMenu = 1;
                    break;
                case 4:
                    inTreeMenu = 1;
                    break;
                case 5:
                    runPriorityQueue();
                    break;
                }
            }
            else if (inStackMenu) {
                switch (stackSelection) {
                case 1:
                    runStackConcept();
                    inStackMenu = 0;
                    break;
                }
            }
            else if (inQueueMenu) {
                switch (queueSelection) {
                case 1:
                    runQueueConcept();
                    inQueueMenu = 0;
                    break;
                case 2:
                    runLinearQueue();
                    inQueueMenu = 0;
                    break;
                case 3:
                    runCircularQueue();
                    inQueueMenu = 0;
                    break;
                case 4:
                    runDeque();
                    inQueueMenu = 0;
                    break;
                }
            }
            else if (inListMenu) {
                switch (listSelection) {
                case 1:
                    runArrayListConcept();
                    inListMenu = 0;
                    break;
                case 2:
                    runLinkedList();
                    inListMenu = 0;
                    break;
                case 3:
                    runSinglyLinkedList();
                    inListMenu = 0;
                    break;
                case 4:
                    runCircularLinkedList();
                    inListMenu = 0;
                    break;
                case 5:
                    runDoublyLinkedList();
                    inListMenu = 0;
                    break;
                }
            }
            else if (inTreeMenu) {
                switch (treeSelection) {
                case 1:
                    runBinaryTree();
                    inTreeMenu = 0;
                    break;
                case 2:
                    runLevelOrderTraversal();
                    inTreeMenu = 0;
                    break;
                case 3:
                    runBinarySearchTree();
                    inTreeMenu = 0;
                    break;
                case 4:
                    runThreadedBinaryTree();
                    inTreeMenu = 0;
                    break;
                }
            }
        }
    }
    return 0;
}
